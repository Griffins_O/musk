//class of main java application login
//main file is visitor.java

package login;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class Visitors extends JFrame {

    public Visitors() {
        setTitle("MUSK");
        setSize(550,400);
        JPanel container=new JPanel();
        JPanel rightPanel=new JPanel();
        JPanel leftPanel=new JPanel();
        
         JPanel namePanel=new JPanel();
        JPanel idPanel=new JPanel();
        JPanel submitPanel=new JPanel();
        JPanel findPanel=new JPanel();
        JPanel logOutPanel=new JPanel();
        JPanel backPanel=new JPanel();
        JPanel allButtons=new JPanel();
        JPanel nextPanel=new JPanel();
        JLabel nameLabel=new JLabel("Enter name        ");
        JTextField nameTextField=new JTextField(20);
        JLabel idLabel=new JLabel("Enter ID number");
        JTextField idField=new JTextField(20);
        JButton submit=new JButton("Submit Details");
        submit.setActionCommand("Submit");
        submit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
               String cmd = e.getActionCommand();
        
                            if(cmd.equals("Submit")){
                            JOptionPane.showMessageDialog(null,"Your details have been saved");
                     }
            }
        });
        JButton find=new JButton("Find");
        find.setActionCommand("Find details");
        find.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
               String cmd = e.getActionCommand();
        
                            if(cmd.equals("Submit")){
                            JOptionPane.showMessageDialog(null,"Records  found");
                     }
                            else{
                                JOptionPane.showMessageDialog(null,"Records not found");
                            }
            }
        });
        JButton logOut=new JButton("Log Out");
        JButton back=new JButton("Previous");
        JButton next=new JButton("Vehicles Tab");
        next.setActionCommand("Next");
       allButtons.setLayout(new FlowLayout());
        logOut.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                System.exit(0);
            }
        });
        JLabel welcome=new JLabel("WELCOME TO EGETON UNIVERSITY",SwingConstants.CENTER);
        getContentPane().add(welcome);
        JLabel welcome2=new JLabel("Visitors Validation",SwingConstants.CENTER);
        getContentPane().add(welcome2);
        back.setActionCommand("Back");
       
        
        back.addActionListener(new ActionListener(){
             public void actionPerformed(ActionEvent e){
                 String cmd = e.getActionCommand();
        
                            if(cmd.equals("Back")){
                            dispose();
                        new LogIn().setVisible(true);
                     }
            }
             
         });
        next.addActionListener(new ActionListener(){
             public void actionPerformed(ActionEvent e){
                 String cmd = e.getActionCommand();
        
                            if(cmd.equals("Next")){
                            dispose();
                        new Vehicles().setVisible(true);
                     }
            }
             
         });
        
        setLayout(new GridLayout(5,1));
        namePanel.add(nameLabel);
        namePanel.add(nameTextField);
         idPanel.add(idLabel);
         idPanel.add(idField);
         findPanel.add(find);
         submitPanel.add(submit);
         logOutPanel.add(logOut);
         backPanel.add(back);
         nextPanel.add(next);
         allButtons.add(submitPanel);
         allButtons.add(findPanel);
         allButtons.add(logOutPanel);
         allButtons.add(backPanel);
         allButtons.add(nextPanel);
         getContentPane().add(namePanel);
         getContentPane().add(idPanel);
         getContentPane().add(allButtons);
         

        rightPanel.add(new JLabel());
        leftPanel.add(new JLabel());

        

JMenuBar menubar = new JMenuBar();
        JMenu options=new JMenu("Options");
        JMenu help=new JMenu("Help");
        options.setMnemonic(KeyEvent.VK_K);
        JMenu print=new JMenu("Print Visitors Records");
        JMenu export=new JMenu("Export Visitors Records as PDF");
        JMenuItem full=new JMenuItem("Full Screen");
        full.setActionCommand("Full Screen");
        full.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent event){
               setExtendedState(MAXIMIZED_BOTH);
        setUndecorated(true);
        setVisible(true);
                
            }
        });
        JMenuItem exit=new JMenuItem("Exit");
        exit.setToolTipText("Exit application");
        //Accelerator key is a key shortcut that launches a menu item//pressing ctrl+w,we close the application
        exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, ActionEvent.CTRL_MASK));
        //exit.addActionListener(this);
        exit.setActionCommand("Exit");
        full.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent event){
               System.exit(0);
            }
        });
        options.add(print);
        options.add(export);
        options.add(full);
        options.add(exit);
        JMenu week=new JMenu("Today");
        JMenu day=new JMenu("This Week");
        JMenu month=new JMenu("This Month");
        
        JMenu week1=new JMenu("Today");
        JMenu day1=new JMenu("This Week");
        JMenu month1=new JMenu("This Month");
        
        JMenuItem content=new JMenuItem("Help Content");
        JMenuItem report=new JMenuItem("Report Issue");
        JMenuItem about=new JMenuItem("About");
        
        print.add(day1);
        print.add(week1);
        print.add(month1);
        
        export.add(day);
        export.add(week);
        export.add(month);
        
        help.add(content);
        help.add(report);
        help.add(about);
        
        menubar.add(options);
        menubar.add(help);
        setJMenuBar(menubar);
        
        
        
setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
setLocationRelativeTo(null);
setVisible(true);
     
   
}
}  
    


    

