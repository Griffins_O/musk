package musk;


import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
/*import com.mongodb.BasicDBObject;	
import com.mongodb.DBCollection;
import com.mongodb.DB;*/
import com.mongodb.MongoClient;
/*import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;*/

public class Database{
	@SuppressWarnings("deprecation")  
	public static void connectDB(String what){
		MongoClient mongoClient = new MongoClient("localhost", 27017); //connection parameters		
		DB db = mongoClient.getDB("musk");				//database name
		if(what == "visitor"){
		DBCollection dBCollection = db.getCollection("visitors");		//collection name = mysql(table)
		BasicDBObject basicDBObject = new BasicDBObject();				
		basicDBObject.put("name", MainVisitor.name);
		basicDBObject.put("ID no", MainVisitor.idNumber);
		dBCollection.insert(basicDBObject);
		mongoClient.close();
		}
		else if(what == "vehicle"){
			DBCollection dBCollection = db.getCollection("vehicles");		//collection name = mysql(table)
			BasicDBObject basicDBObject = new BasicDBObject();				
			basicDBObject.put("name", Vehicles.name);
			basicDBObject.put("ID no", Vehicles.idNumber);
			basicDBObject.put("ID no", Vehicles.regNo);
			dBCollection.insert(basicDBObject);
			mongoClient.close();
		}
		else{
			System.exit(0);
		}
		
	}
	
}


// "name" : "value"

/*public class Database {
	private String url = "localhost";
	private int port = 27017;
	private String database = "musk";
	public	
	MongoClient mongoClient;
	MongoDatabase db;
	void connectDB(){
		mongoClient = new MongoClient(url, port);
		db = mongoClient.getDatabase(database);	
		DBCollection dbCollection = db.getCollection("CHannel");
		BasicDBObject basicDBObject = new BasicDBObject();
		basicDBObject.put("name", "Griffins");
		basicDBObject.put("Subscriptions", 4100);
		dbCollection.insert(basicDBObject);
	}	
}*/