
package login;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class LogIn extends JFrame implements ActionListener{

    public LogIn() {
        setTitle("MUSK");
        setSize(600,400);
        JMenuBar menubar = new JMenuBar();
        JMenu options=new JMenu("Options");
        JMenu help=new JMenu("Help");
        options.setMnemonic(KeyEvent.VK_K);
        JMenu print=new JMenu("Print Visitors Records");
        JMenu export=new JMenu("Export Visitors Records as PDF");
        JMenuItem full=new JMenuItem("Full Screen");
        full.setActionCommand("Full Screen");
        full.addActionListener(new ActionListener(){
           
            public void actionPerformed(ActionEvent event){
               setExtendedState(MAXIMIZED_BOTH);
        setUndecorated(true);
        setVisible(true);
                
            }
        });
        JMenuItem exit=new JMenuItem("Exit");
        exit.setToolTipText("Exit application");
        //Accelerator key is a key shortcut that launches a menu item//pressing ctrl+w,we close the application
        exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, ActionEvent.CTRL_MASK));
        exit.addActionListener(this);
        exit.setActionCommand("Exit");
        options.add(print);
        options.add(export);
        options.add(full);
        options.add(exit);
        JMenu week=new JMenu("Today");
        JMenu day=new JMenu("This Week");
        JMenu month=new JMenu("This Month");
        JMenu week1=new JMenu("Today");
        JMenu day1=new JMenu("This Week");
        JMenu month1=new JMenu("This Month");
        JMenuItem content=new JMenuItem("Help Content");
        JMenuItem report=new JMenuItem("Report Issue");
        JMenuItem about=new JMenuItem("About");
        
        print.add(day1);
        print.add(week1);
        print.add(month1);
        
        export.add(day);
        export.add(week);
        export.add(month);
        
        help.add(content);
        help.add(report);
        help.add(about);
        
        menubar.add(options);
        menubar.add(help);
        setJMenuBar(menubar);
        
        JPanel usernamePanel=new JPanel();
        JPanel passwordPanel=new JPanel();
        JPanel logInPanel=new JPanel();
        JPanel logOutPanel=new JPanel();
        JPanel logTimePanel=new JPanel();
        
        JLabel statement=new JLabel("SECURITY PERSONELL ON DUTY",SwingConstants.CENTER);
        
        JLabel usernameLabel=new JLabel("         Username ");
        JTextField  nameTextField=new JTextField(20);
        JLabel passwordLabel=new JLabel("Password");
        JPasswordField passField=new JPasswordField(20);
        //get the password
        char[] password=passField.getPassword();
        JCheckBox hidePasswordCheckBox=new JCheckBox("Show Password");
        JPanel boxPanel=new JPanel();
        JButton login=new JButton("Login");
        JButton logOut=new JButton("Log Out");
         JLabel logTime=new JLabel("Logged in at:");
        logOut.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                System.exit(0);
            }
        });
        
        setLayout(new GridLayout(7,1));
        JPanel f_passPanel=new JPanel();
        f_passPanel.setLayout(new FlowLayout(4,5,1));
        usernamePanel.add(usernameLabel);
        usernamePanel.add(nameTextField);
        passwordPanel.add(passwordLabel);
        passwordPanel.add(passField);
        passwordPanel.add(hidePasswordCheckBox);
        f_passPanel.add(passwordPanel);
        logInPanel.add(login);
        logOutPanel.add(logOut);
        
        logTimePanel.add(logTime);
        
        getContentPane().add(statement);
        getContentPane().add(usernamePanel);
        getContentPane().add(f_passPanel);
        //getContentPane().add(hidePasswordCheckBox);
        getContentPane().add(logInPanel);
        getContentPane().add(logOutPanel);
        getContentPane().add(logTimePanel);
        
        login.addActionListener(this);
        login.setActionCommand("Open");
        
        
        hidePasswordCheckBox.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e){
                if(e.getStateChange()==ItemEvent.SELECTED){
                    passField.setEchoChar((char)0);
                }
                else{
                    passField.setEchoChar(' ');
                }
            }
        });
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
     }
       
         public void actionPerformed(ActionEvent e){
        String cmd = e.getActionCommand();
        
        if(cmd.equals("Open")){
            dispose();
            new Visitors();
        }
    }
          public void actionPerformed1(ActionEvent e){
        String cmd = e.getActionCommand();
        
        if(cmd.equals("Open")){
            dispose();
            new Vehicles();
        }
    }
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable(){
            public void run(){
                new LogIn().setVisible(true);
            }
        });
    }
    
}
